package converter;


import java.util.LinkedList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Converter {
    public abstract String getName();

    public abstract String convert(String text);


    protected Pair getPair(RegEx regEx, String text, Func func) {

        List<String> fromList = new LinkedList<>();
        List<String> toList = new LinkedList<>();

        String[] strings = regEx.get();

        for (String regex : strings) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(text);
            int from = 0;
            while (matcher.find(from)) {
                MatchResult result = matcher.toMatchResult();
                String group = result.group();
                fromList.add(group);
                toList.add(func.to(group));
                from = result.end();
            }
        }

        Pair pair = new Pair();
        pair.first = new String[fromList.size()];
        pair.first = fromList.toArray(pair.first);

        pair.second = new String[toList.size()];
        pair.second = toList.toArray(pair.second);

        return pair;

    }


    private int seq = 0;

    protected String getUnique() {
        return String.format("{*#*%d*#*}", ++seq);
    }


    protected class Pair {
        public String[] first;
        public String[] second;
    }

    protected interface Func {
        String to(String s);
    }


}