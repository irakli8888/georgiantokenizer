package converter;


import java.util.ArrayList;
import java.util.Scanner;

public class RegEx {

    String[] regexps;

    private RegEx(String fileName) {
        Scanner s = new Scanner(RegEx.class.getClassLoader().getResourceAsStream(fileName));

        ArrayList<String> list = new ArrayList<>();
        while (s.hasNext()) {
            list.add(s.next());
        }

        s.close();

        this.regexps = new String[list.size()];
        regexps = list.toArray(regexps);

    }

    public static RegEx create(String fileName) {
        return new RegEx(fileName);
    }

    public String[] get() {
        return regexps;
    }
}